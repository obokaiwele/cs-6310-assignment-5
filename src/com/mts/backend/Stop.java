package com.mts.backend;

public class Stop {
    public String id = "";
    public String name = "";
    public Integer riders = 0;
    public Location location = null;

    public Stop(String id, String name, Integer riders, Location location) {
        this.id = id;
        this.name = name;
        this.riders = riders;
        this.location = location;
    }

    public void add_riders(Integer riders) {
        this.riders += riders;
    }

    public void remove_riders(Integer riders) {
        this.riders -= riders;
    }
}
