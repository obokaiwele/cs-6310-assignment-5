package com.mts;

import com.mts.backend.*;

import java.io.*;
import java.util.*;


public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
//        String inputPath = "/home/ob/dev/cs-6310/2018.10.20_-_Assignment_-_05/test0_instruction_demo.txt";
        String inputPath = args[0];
        List<List<String>> scenario = Util.createScenario(inputPath);
        Simulation simulation = Util.createSimulation(scenario);
        Util.runSimulation(simulation);
    }
}