package com.mts.backend;

public class Bus {
    public String id;
    public Route route;
    public Integer location;
    public Integer passengers;
    public Integer passengerCapacity;
    public Integer fuel;
    public Integer fuelCapacity;
    public Integer speed;

    public Bus(String id, Route route, Integer location, Integer passengers, Integer passengerCapacity, Integer fuel, Integer fuelCapacity, Integer speed) {
        this.id = id;
        this.route = route;
        this.location = location;
        this.passengers = passengers;
        this.passengerCapacity = passengerCapacity;
        this.fuel = fuel;
        this.fuelCapacity = fuelCapacity;
        this.speed = speed;
    }

    /**
     * Get the next Stop in Bus Route. If current Stop is the last in Route, next Stop is first in Route
     * @return stop
     */
    public Stop get_next_stop() {
        int numStops = this.route.stops.size();
        int location = (this.location + 1) % numStops;
        return this.route.stops.get(location);
    }

    public Stop get_current_stop() {
        return this.route.stops.get(location);
    }

    public Time get_next_stop_arrival_time(Time currentTime) {
        Location currentLocation = get_current_stop().location;
        Location nextLocation = get_next_stop().location;
        Integer travelTime = currentLocation.calculate_travel_time(nextLocation, this.speed);
        return new Time(currentTime.value + travelTime);
    }

    /**
     * Move bus to next stop along Route, circling back to first Stop after last Stop in Route
     */
    public void move_bus() {
        int numStops = this.route.stops.size();
        this.location  = (this.location + 1) % numStops;
    }

    public void add_passengers(Integer passengers) {
        this.passengers += passengers;
    }

    public void remove_passengers(Integer passengers) {
        this.passengers -= passengers;
    }
}
