package com.mts.backend;

public class Event {
    public String id;
    public Time time;
    public EventType type;

    public Event(Time time, EventType type, String id) {
        this.time = time;
        this.type = type;
        this.id = id;
    }

    public enum EventType {
        MOVE_BUS;
    }
}
