package com.mts.backend;

public class Depot {
    public String id = "";
    public String name = "";
    public Location location = null;

    public Depot(String id, String name, Location location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }
}
