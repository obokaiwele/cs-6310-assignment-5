# CS6310 - MTS

## Copy code to local machine
`git clone https://obokaiwele@bitbucket.org/obokaiwele/cs-6310-assignment-5.git`

## Compile code
- `cd ./src`
- `make`

## Run tests
- `make run0`
- `make run1`
- `make all`

## Cleanup compiles files and test results 
- `make clean`
