package com.mts.backend;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Simulation {
    public Map<String, Stop> stops = new HashMap<>();
    public Map<String, Bus> buses = new HashMap<>();
    public Map<String, Depot> depots = new HashMap<>();
    public PriorityQueue<Event> events = new PriorityQueue<Event>(10, new EventTimeComparator());
    public Map<String, Route> routes = new HashMap<>();

    public Simulation() {

    }

    public void add_stop(Stop stop) {
        this.stops.put(stop.id, stop);
    }

    public void add_bus(Bus bus) {
        this.buses.put(bus.id, bus);
    }

    public void add_depot(Depot depot) {
        this.depots.put(depot.id, depot);
    }

    public void add_event(Event event) {
        this.events.add(event);
    }

    public void add_route(Route route) {
        this.routes.put(route.id, route);
    }
}
