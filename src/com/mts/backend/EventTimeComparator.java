package com.mts.backend;

import java.util.Comparator;

public class EventTimeComparator implements Comparator<Event> {
    public int compare(Event e1, Event e2) {
        return e1.time.value - e2.time.value;
    }
}
