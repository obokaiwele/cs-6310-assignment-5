package com.mts.backend;

import com.mts.Main;

public class Location {
    Double longitude;
    Double latitude;

    public Location(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Double calculate_distance(Location location) {
        Double CONVERSION_FACTOR = 70.0;
        Double x = this.longitude - location.longitude;
        Double y = this.latitude - location.latitude;
        return CONVERSION_FACTOR * Math.sqrt((x * x) + (y * y));
    }

    public Integer calculate_travel_time(Location location, Integer speed) {
        Double distance = this.calculate_distance(location);
        return 1 + ( distance.intValue() * 60 / speed );
    }
}
