package com.mts.backend;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Util {
    public String id = "";
    public String name = "";
    public Location location = null;

    public Util(String id, String name, Location location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public static Depot createDepot(List<String> item) {
        Double longitude = Double.parseDouble(item.get(3));
        Double latitude = Double.parseDouble(item.get(4));
        Location location = new Location(longitude, latitude);
        Depot depot = new Depot(item.get(1), item.get(2), location);
        return depot;
    }

    public static Stop createStop(List<String> item) {
        Double longitude = Double.parseDouble(item.get(4));
        Double latitude = Double.parseDouble(item.get(5));
        Location location = new Location(longitude, latitude);
        Integer riders = Integer.parseInt(item.get(3));
        Stop stop = new Stop(item.get(1), item.get(2), riders, location);
        return stop;
    }

    public static Route createRoute(List<String> item) {
        Integer number = Integer.parseInt(item.get(2));
        Route route = new Route(item.get(1), number, item.get(3));
        return route;
    }

    public static Bus createBus(List<String> item, Simulation simulation) {
        Route route = simulation.routes.get(item.get(2));
        Integer location = Integer.parseInt(item.get(3));

        Integer passengers = Integer.parseInt(item.get(4));
        Integer passengerCapacity = Integer.parseInt(item.get(5));

        Integer fuel = Integer.parseInt(item.get(6));
        Integer fuelCapacity = Integer.parseInt(item.get(7));

        Integer speed = Integer.parseInt(item.get(8));

        Bus bus = new Bus(item.get(1), route, location, passengers, passengerCapacity, fuel, fuelCapacity, speed);
        return bus;
    }

    public static Event createEvent(List<String> item) {
        Integer timeValue = Integer.parseInt(item.get(1));
        Time time = new Time(timeValue);

        Event.EventType type = null;
        switch(item.get(2)) {
            case "move_bus":
                type = Event.EventType.MOVE_BUS;
        }

        Event event = new Event(time, type, item.get(3));
        return event;
    }

    public static Simulation createSimulation(List<List<String>> scenario) {
        Simulation simulation = new Simulation();

        for (List<String> item : scenario) {
            switch(item.get(0)) {
                case "add_depot":
                    Depot depot = createDepot(item);
                    simulation.add_depot(depot);
                    break;
                case "add_stop":
                    Stop stop = createStop(item);
                    simulation.add_stop(stop);
                    break;
                case "add_route":
                    Route route = createRoute(item);
                    simulation.add_route(route);
                    break;
                case "add_bus":
                    Bus bus = createBus(item, simulation);
                    simulation.add_bus(bus);
                    break;
                case "add_event":
                    Event event = createEvent(item);
                    simulation.add_event(event);
                    break;
                case "extend_route":
                    Route eRoute = simulation.routes.get(item.get(1));
                    Stop eStop = simulation.stops.get(item.get(2));
                    eRoute.extend_route(eStop);
                    break;
                default:
                    System.out.println("default: " + item.get(0));
                    break;
            }
        }

        return simulation;
    }

    public static List<List<String>> createScenario(String path) throws FileNotFoundException, IOException {
        List<List<String>> scenario = new ArrayList<>();

        File file = new File(path);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String string;
        while ((string = reader.readLine()) != null) {
            List<String> items = Arrays.asList(string.split(","));
            scenario.add(items);
        }

        return scenario;
    }

    public static void runSimulation(Simulation simulation) {
        int MOVE_BUS_LIMIT = 20;    // Maximum  number of MOVE_BUS events in a Simulation
        int numMoves = 0;

        while( ! simulation.events.isEmpty() && numMoves < MOVE_BUS_LIMIT ) {
            Event event = simulation.events.poll();
            Bus bus = simulation.buses.get(event.id);
            Stop stop = bus.get_next_stop();
            Time currentTime = event.time;
            Time nextTime = bus.get_next_stop_arrival_time(currentTime);
            bus.move_bus();

            Event nextEvent = new Event(nextTime, Event.EventType.MOVE_BUS, bus.id);
            simulation.events.add(nextEvent);
            numMoves += 1;

            String output = String.format("b:%s->s:%s@%d//p:%d/f:%d", bus.id, stop.id, nextTime.value, bus.passengers, 0);
            System.out.println(output);
        }
    }
}
