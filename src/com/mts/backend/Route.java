package com.mts.backend;

import java.util.ArrayList;

public class Route {
    public String id = "";
    public Integer number = 0;
    public String name = "";
    public ArrayList<Stop> stops = new ArrayList<Stop> ();

    public Route(String id, Integer number, String name) {
        this.id = id;
        this.number = number;
        this.name = name;
    }

    public void extend_route(Stop stop) {
        this.stops.add(stop);
    }
}
